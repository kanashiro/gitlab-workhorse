image: golang:1.9

include:
  - template: Code-Quality.gitlab-ci.yml

variables:
  REPO_NAME: gitlab.com/gitlab-org/labkit

stages:
  - build
  - verify

.job_template: &link_to_gopath
  before_script:
    - mkdir -p $GOPATH/src/$(dirname $REPO_NAME)
    - ln -svf $CI_PROJECT_DIR $GOPATH/src/$REPO_NAME
    - cd $GOPATH/src/$REPO_NAME

.job_template: &build_template
  <<: *link_to_gopath
  stage: build
  script:
    - ./compile.sh

build_1.9:
  image: golang:1.9
  <<: *build_template

build_1.10:
  image: golang:1.10
  <<: *build_template

build_1.11:
  image: golang:1.11
  <<: *build_template

build_1.12:
  image: golang:1.12
  <<: *build_template

build_1.13:
  image: golang:1.13
  <<: *build_template

test_1.9:
  stage: verify
  <<: *link_to_gopath
  script:
    - ./test.sh

test_1.10:
  stage: verify
  image: golang:1.10
  <<: *link_to_gopath
  script:
    - ./test.sh

test_1.11:
  stage: verify
  image: golang:1.11
  <<: *link_to_gopath
  script:
    - ./test.sh

test_1.12:
  stage: verify
  image: golang:1.12
  <<: *link_to_gopath
  script:
    - ./test.sh

test_1.13:
  stage: verify
  image: golang:1.13
  <<: *link_to_gopath
  script:
    - ./test.sh

# The verify step should always use the same version of Go as devs are
# likely to be developing with to avoid issues with changes in these tools
# between go versions. Since these are simply linter warnings and not
# compiler issues, we only need a single version
verify:
  stage: verify
  image: golang:1.11
  <<: *link_to_gopath
  script:
    - ./vet.sh

sast:
  stage: verify
  image: docker:stable
  variables:
    DOCKER_DRIVER: overlay2
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
        --env SAST_CONFIDENCE_LEVEL="${SAST_CONFIDENCE_LEVEL:-3}"
        --env GO111MODULE=on
        --volume "$PWD:/code"
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/security-products/sast:$SP_VERSION" /app/bin/run /code
  artifacts:
    reports:
      sast: gl-sast-report.json

code_quality:
  stage: verify

# Ensure that all the changes are backwards compatible with GitLab Workhorse
backwards_compat_workhorse:
  stage: verify
  image: golang:1.13
  script:
    - mkdir -p $GOPATH/src/gitlab.com/gitlab-org/
    - git clone https://gitlab.com/gitlab-org/gitlab-workhorse.git $GOPATH/src/gitlab.com/gitlab-org/gitlab-workhorse
    - cd $GOPATH/src/gitlab.com/gitlab-org/gitlab-workhorse
    - GO111MODULE=on go get -u -a
    - GO111MODULE=on go get -u=patch gitlab.com/gitlab-org/labkit@$CI_COMMIT_REF_NAME
    - make

# Ensure that all the changes are backwards compatible with GitLab Workhorse
backwards_compat_gitaly:
  stage: verify
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.6-golang-1.13-git-2.22
  script:
    - mkdir -p $GOPATH/src/gitlab.com/gitlab-org/
    - git clone https://gitlab.com/gitlab-org/gitaly.git $GOPATH/src/gitlab.com/gitlab-org/gitaly
    - cd $GOPATH/src/gitlab.com/gitlab-org/gitaly
    - GO111MODULE=on go get -u=patch gitlab.com/gitlab-org/labkit@$CI_COMMIT_REF_NAME
    - make

mod tidy:
  stage: verify
  image: golang:1.13
  script:
    - ./tidy.sh
