gitlab-workhorse (8.32.0+debian-1) unstable; urgency=medium

  * New upstream version 8.32.0+debian
  * Add golang-github-alecthomas-chroma-dev to Build-Depends

 -- Pirate Praveen <praveen@debian.org>  Sat, 23 May 2020 15:22:39 +0530

gitlab-workhorse (8.30.1+debian-1) unstable; urgency=medium

  * Use golang-github-client9-reopen-dev as build dependency
    and drop from vendor
  * Drop Files-Excluded from copyright (vendor is manually created now)
  * New upstream version 8.30.1+debian

 -- Pirate Praveen <praveen@debian.org>  Wed, 22 Apr 2020 21:45:29 +0530

gitlab-workhorse (8.30+debian-1) unstable; urgency=medium

  * Update build dependencies for buster-backports build
  * New upstream version 8.30+debian
  * Exclude cable_test.go from tests (needs definitions from other excluded
    tests)

 -- Pirate Praveen <praveen@debian.org>  Mon, 06 Apr 2020 00:35:37 +0530

gitlab-workhorse (8.18.0+debian-1) unstable; urgency=medium

  [ Dmitry Smirnov ]
  * vendorworks: reduce vendoring, strict clean-up as per Files-Excluded.
  * Enabled some tests
  * Build-Depends:
    - golang-github-dgrijalva-jwt-go-v3-dev
    + golang-github-dgrijalva-jwt-go-dev (>= 3.2.0~)
    + golang-github-gomodule-redigo-dev
    + golang-github-jfbus-httprs-dev
    + golang-github-sebest-xff-dev
    + libimage-exiftool-perl

  [ Pirate Praveen ]
  * New upstream version 8.18.0+debian
  * Change netstat autopkgtest dependency to net-tools (fixes autopkgtest
    regression)

 -- Pirate Praveen <praveen@debian.org>  Wed, 01 Jan 2020 17:39:44 +0530

gitlab-workhorse (8.16.0+debian-2) unstable; urgency=low

  [ Adrian Bunk ]
  * Update the build dependency from golang-logrus-dev
    to golang-github-sirupsen-logrus-dev. (Closes: #946022)
  * Add test dependency on netstat. (Closes: #931662)

 -- Pirate Praveen <praveen@debian.org>  Tue, 31 Dec 2019 14:56:45 +0530

gitlab-workhorse (8.16.0+debian-1) unstable; urgency=medium

  * Add README.source to explain how to recreate orig.tar
  * New upstream version 8.16.0+debian
  * Don't clean vendor debian/rules or exclude in debian/copyright (it is
    manually created now)

 -- Pirate Praveen <praveen@debian.org>  Mon, 09 Dec 2019 21:33:49 +0530

gitlab-workhorse (8.8.1+debian-3) unstable; urgency=medium

  * Add upstream patch for rate limiting (Fixes: CVE-2019-19260)

 -- Pirate Praveen <praveen@debian.org>  Fri, 29 Nov 2019 00:13:24 +0530

gitlab-workhorse (8.8.1+debian-2) unstable; urgency=medium

  * Reupload to unstable (gitlab in unstable cannot be supported meaningfully)

 -- Pirate Praveen <praveen@debian.org>  Sun, 17 Nov 2019 15:06:19 +0530

gitlab-workhorse (8.8.1+debian-1) experimental; urgency=medium

  * New upstream version 8.8.1+debian
  * Fix package wrt cme
  * Bump Standards-Version to 4.4.0
  * Bump debhelper-compat to 12

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 22 Oct 2019 18:01:42 +0530

gitlab-workhorse (8.7.0+debian-1) experimental; urgency=medium

  * New upstream version 8.7.0+debian
  * Bump Standards-Version to 4.4.0 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Sat, 03 Aug 2019 11:11:59 +0000

gitlab-workhorse (8.5.2+debian-1) experimental; urgency=medium

  * Team upload
  * New upstream version 8.5.2+debian
  * Bump Standards-Version to 4.3.0 (no changes needed)

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Wed, 15 May 2019 17:34:32 +0530

gitlab-workhorse (7.6.0+debian-1) unstable; urgency=medium

  * New upstream version 7.6.0+debian
  * Set minimum version of golang-golang-x-sys-dev
  * Remove unused overrides

 -- Pirate Praveen <praveen@debian.org>  Tue, 18 Dec 2018 18:31:19 +0530

gitlab-workhorse (7.4.0+debian-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Sat, 08 Dec 2018 12:34:45 +0530

gitlab-workhorse (7.4.0+debian-1) experimental; urgency=medium

  * New upstream version 7.4.0+debian
  * Refresh patches
  * Use embedded raven-go and grpc (from vendor directory, the packaged
    versions need updates)

 -- Pirate Praveen <praveen@debian.org>  Wed, 05 Dec 2018 20:47:55 +0530

gitlab-workhorse (6.1.1+debian-3) unstable; urgency=medium

  * Add an autopkgtest to check if gitlab-workhorse is starting

 -- Pirate Praveen <praveen@debian.org>  Sat, 24 Nov 2018 07:41:31 +0530

gitlab-workhorse (6.1.1+debian-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Thu, 22 Nov 2018 13:29:23 +0530

gitlab-workhorse (6.1.1+debian-1) experimental; urgency=medium

  * New upstream version 6.1.1+debian

 -- Pirate Praveen <praveen@debian.org>  Wed, 21 Nov 2018 11:47:04 +0530

gitlab-workhorse (5.2.0+debian-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Sat, 17 Nov 2018 12:59:57 +0530

gitlab-workhorse (5.2.0+debian-1) experimental; urgency=medium

  * New upstream version
  * Standard version updated

 -- Manas Kashyap <manaskashyaptech@gmail.com>  Sat, 11 Aug 2018 05:35:21 +0000

gitlab-workhorse (4.3.1+debian-1) unstable; urgency=medium

  * New upstream version

 -- Manas Kashyap <manaskashyaptech@gmail.com>  Mon, 18 Jun 2018 23:52:53 +0000

gitlab-workhorse (4.2.1+debian-2) unstable; urgency=medium

  * Set minimum version of golang-any to 2:1.10~ (to help with
    stretch-backports)

 -- Pirate Praveen <praveen@debian.org>  Sun, 03 Jun 2018 20:50:11 +0530

gitlab-workhorse (4.2.1+debian-1) unstable; urgency=medium

  * New upstream version 4.2.1+debian
  * Fix watch file pattern

 -- Pirate Praveen <praveen@debian.org>  Sun, 03 Jun 2018 20:35:30 +0530

gitlab-workhorse (4.0.0+debian-1) unstable; urgency=medium

  * New upstream version 4.0.0
  * Switch vcs to go-team

 -- Pirate Praveen <praveen@debian.org>  Wed, 28 Mar 2018 12:20:34 +0530

gitlab-workhorse (3.8.0+debian-1) unstable; urgency=medium

  [ Michael Stapelberg ]
  * Switch to XS-Go-Import-Path

  [ Pirate Praveen ]
  * Update watch file for new download url pattern
  * New upstream version 3.8.0
  * Refresh patches

 -- Pirate Praveen <praveen@debian.org>  Fri, 16 Mar 2018 19:11:00 +0530

gitlab-workhorse (0.8.5+debian-3) unstable; urgency=medium

  * Manually build on s390x (Closes: #843148)

 -- Pirate Praveen <praveen@debian.org>  Fri, 04 Nov 2016 17:09:13 +0530

gitlab-workhorse (0.8.5+debian-2) unstable; urgency=medium

  * Remove optional go-compiler from build dependency

 -- Pirate Praveen <praveen@debian.org>  Thu, 03 Nov 2016 11:44:07 +0530

gitlab-workhorse (0.8.5+debian-1) unstable; urgency=medium

  * New upstream release
  * Switch build dep to golang-github-dgrijalva-jwt-go-v3-dev (Closes #841617)

 -- Pirate Praveen <praveen@debian.org>  Tue, 01 Nov 2016 10:52:16 +0530

gitlab-workhorse (0.8.2+debian1-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Tue, 11 Oct 2016 16:01:41 +0530

gitlab-workhorse (0.8.2+debian1-1) experimental; urgency=medium

  * Use packaged raven-go and jwt-go (fix build failure in powerpc)

 -- Pirate Praveen <praveen@debian.org>  Mon, 10 Oct 2016 12:42:47 +0530

gitlab-workhorse (0.8.2-1) unstable; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Fri, 30 Sep 2016 21:51:27 +0530

gitlab-workhorse (0.7.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Standards-Version: 3.9.8.
  * Build using "dh-golang"; added "Built-Using" field, etc.

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 18 Jul 2016 20:01:41 +1000

gitlab-workhorse (0.6.3-1) unstable; urgency=medium

  * New upstream release
  * Drop patch: modify-paths.patch (merged upstream)

 -- Pirate Praveen <praveen@debian.org>  Wed, 10 Feb 2016 00:02:46 +0530

gitlab-workhorse (0.5.0-1) unstable; urgency=low

  * Initial release (Closes: #808172)

 -- Pirate Praveen <praveen@debian.org>  Wed, 16 Dec 2015 12:32:37 +0530
